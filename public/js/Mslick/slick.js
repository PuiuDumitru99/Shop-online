console.log("Js file slick Connectetd");


function start() {
    $('.wrapper_Bord').slick({
        dots:false,
    });

    $('.wrapper_product').slick({
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 5,
        responsive:[
            {
                breakpoint: 1040,
                settings: {
                slidesToShow: 5,
                slidesToScroll: 5,}
            },
            {
                breakpoint: 800,
                settings: {
                slidesToShow: 3,
                slidesToScroll: 3,}
            },

            {
                breakpoint: 600,
                settings: {
                slidesToShow: 3,
                slidesToScroll: 3,}
            },
            {
                breakpoint: 480,
                settings: {
                slidesToShow: 2,
                slidesToScroll: 2,}
            },
            {
                breakpoint: 200,
                settings: {
                slidesToShow: 1,
                slidesToScroll: 1,}
            },
        ]
    });
    $('.banner').slick({
        arrows:false,
        dots:true,
    });

    $('.img_product').slick({
        dots:true,
        arrows:false,
    });
    console.log("start slick");    
}
start();