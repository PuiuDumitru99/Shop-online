<?php

namespace App\Http\Controllers;

use App\Models\bilboard as Modelsbilboard;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    //

    public function index(){
        
       $bilboards=Modelsbilboard::query()->get('name_img');
       
       
        return view("home.index",compact('bilboards'));

    }
}
