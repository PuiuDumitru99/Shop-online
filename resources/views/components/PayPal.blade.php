<button class="paypal" {{$attributes}} >
    {{$slot}}
    <img src="/img/PayPal.png" alt="">
</button>
<style>
    .paypal{
        padding: 1px 5px 1px 5px;
        height: 35px;   
        width: 100%;     
        text-decoration: none !important;
        text-decoration-line: none;
        text-transform: none;
        border-radius: 50px;
        display: flex;
        justify-content: space-evenly;
        align-items:center;
        text-align: center;
        font-size: 14px;
        font-weight: 600;
        background: #FFB800;
        border:none;
        color:#232C65;
    }
    .paypal:hover{
        border:2px solid #FFB800;
        background-color: white !important;
    }
    .paypal>img{
       height: 75%;
       width: auto;
       margin-left: 5px       
    }

</style>