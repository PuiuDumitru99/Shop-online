<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/header.css">
    <link rel="stylesheet" href="./css/footer.css">
    <title>Document</title>
    @stack('css')
    @vite(['resources/css/app.css','resources/js/app.js'])
</head>
<body>
    <header>
        @include('includes.header')
    </header>
    <main>
       
        <div class="container mt-1">
            @yield('content') 
        </div>
        
    </main>
    <div class="wrapper_opption">
        <div class="container">
            <div class="row">
                <div class="col-12 d-flex justify-content-around align-items-center p-1">
                    <div class="col col-4 d-flex flex-column  justify-content-around  align-items-center support">
                        <a href="http://"> </a>
                        <span>
                            Product Support
                        </span>
                        <span>Up to 3 years on-site warranty available for your peace of mind.</span>
                    </div>
                    <div class="col col-4 d-flex flex-column  justify-content-around  align-items-center account ">
                        <a href="http://"></a>
                        <span>Personal Account</span>
                        <span>With big discounts, free delivery and a dedicated support specialist.</span>
                    </div>
                    <div class=" col col-4  d-flex flex-column  justify-content-around  align-items-center saving" >
                        <a href="http://" ></a>
                        <span>Amazing Savings</span>
                        <span>Up to 70% off new Products, you can be sure of the best price.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
        @include('includes.footer')
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
<script src="./js/script.js">
</script>
@stack('js')
</html>