<div class="summary" {{$attributes}}>
    {{$slot}}

</div>
<style>
    .summary{
    height: 85%;
    background: #F5F9FF;
    padding-left: 20px;
    padding-right: 20px;
    box-sizing:border-box;
    display: grid;
}
</style>