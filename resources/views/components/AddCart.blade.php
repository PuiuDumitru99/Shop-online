<button class="addCart" {{$attributes}}>
    {{$slot}}
</button>

<style>
    .addCart{
        padding: 1px 5px 1px 5px;
        height: 35px;
        width: 100%;
        border:none;
        color: white !important;
        background: #0156FF;        
        text-decoration: none !important;
        text-decoration-line: none;
        text-transform: none;
        border-radius: 50px;
        display: flex;
        justify-content: center;
        align-items:center;
        text-align: center;
        font-size: 14px;
        font-weight: 600;
    }
    .addCart:hover{
        border: 2px solid #0156FF;
        color:#0156FF !important; 
        background: transparent;

    }

</style>