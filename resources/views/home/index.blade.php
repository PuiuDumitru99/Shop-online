@extends('components.base')
@push('css')
    <link rel="stylesheet" href="./css/home.css">
    <link rel="stylesheet" href="./css/slick/slick.css">
    <link rel="stylesheet" href="./css/slick/slick-theme.css">
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="wrapper_Bord">    
            @foreach ($bilboards as $bilboard)
            <img src="./img/bilbord/{{$bilboard->name_img}}.png" class="item1" alt="">
            @endforeach   
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12 d-flex justify-content-between align-items-center mt-3 mb-3" >
        <b>New Products</b>
        <a href="http://"> See All New Products</a>
    </div>
</div>
<div class="row">
    <div class="col col-12 d-flex justify-content-center align-items-center">
        <div class="wrapper_product">
            @for ($i = 1; $i <=6; $i++)
            <div class=" col-sm-2 product pl-2 pr-2">
                <span>In Stock</span>
                <img class="img_product" src="./img/img_prod/{{$i}}.png" alt="">
                <div class="rating d-flex justify-content-between align-items-center pl-1 pr-1">
                   <div class="star d-flex">
                    @for ($s =0 ; $s < 5; $s++)
                    <img class="star" src="./img/star.svg" alt="">
                    @endfor
                   </div>
                   <div class="rew">
                    Reviews ({{$i}})
                   </div>
                </div>
                <div class="content mt-2">
                    EX DISPLAY : MSI Pro 16 Flex-036AU 15.6 MULTITOUCH All-In-On...
                </div>
                <div class="price mt-2">
                    <p>$490.00</p>
                    <b>$490.00</b>
                </div>
            </div>
            @endfor
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12 d-flex  justify-content-center align-items-center zip" >
        <img src="/img/zip.svg" alt="">
        <span>|</span>
        <span>own it now, up to 6 months interest free </span>
        <a href="http://">learn more</a>
    </div>
</div>
@for ($k =0 ; $k <5 ; $k++)
<div class="row">
    <div class="col-12 d-flex">
        <div class=" col-2 custom_Bil">
            <b>
                Custome Builds
            </b>
            <a href="http://">
                See All Products
            </a>
        </div>
        <div class="col d-flex wrapper_product_categry">
            @for ($i = 1; $i <=5; $i++)
            <div class="product">
                <span>In Stock</span>
                <img class="img_product" src="./img/img_prod/{{$i}}.png" alt="">
                <div class="rating d-flex justify-content-between align-items-center pl-1 pr-1">
                    <div class="star d-flex">
                    @for ($s =0 ; $s < 5; $s++)
                    <img class="star" src="./img/star.svg" alt="">
                    @endfor
                    </div>
                    <div class="rew">
                    Reviews ({{$i}})
                    </div>
                </div>
                <div class="content mt-2">
                    EX DISPLAY : MSI Pro 16 Flex-036AU 15.6 MULTITOUCH All-In-On...
                </div>
                <div class="price mt-2">
                    <p>$490.00</p>
                    <b>$490.00</b>
                </div>
            </div>
            @endfor
        </div>
    </div>
</div>
@endfor
<div class="row">
    <x-brand>
        
    </x-brand>
</div>
<div class="row d-flex justify-content-center">
    <div class="col-10 review m-5 p-4">
        <div class="row">
            <span class="col-2 nowrap justify-content-end">"</span>
           <p class="col">
            My first order arrived today in perfect condition. From the time I sent a question about the item to making the purchase, to the shipping and now the delivery, your company, Tecs, has stayed in touch. Such great service. I look forward to shopping on your site in the future and would highly recommend it.
           </p>
        </div>
        <span>
            Tama Brown
        </span>
        <div class="row d-flex justify-content-between">
            <button>
                Leave Us A Review
            </button>

        </div>

    </div>
</div>
</div>
@push('js')
<script src="./js/Mslick/slick.min.js">
</script>
<script src="./js/Mslick/slick.js">
</script>
@endpush
@endsection