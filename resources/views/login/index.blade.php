@extends('components.base')

@push('css')
<link rel="stylesheet" href="/css/login.css">
    
@endpush
@section('content')
<div class="row">
    <div class="col-12 mt-3">
        <span class="Tpage">Customer Login</span>
    </div>
</div>
<div class="row min-vh-100 ">

    <div class="col-12 gap-4  pt-5 d-flex justify-content-center ">
            <div class="col-4">
            <form class="px-4 py-3 d-grid">         
                <b>
                    Registered Customers
                </b>      
                <span >
                    If you have an account, sign in with your email address.
                </span> 
                <div class="form-group">
                  <label for="exampleDropdownFormEmail1">Email address</label>
                  <input type="email" required class="form-control" id="exampleDropdownFormEmail1" placeholder="Your Name">
                </div>
                <div class="form-group">
                  <label for="exampleDropdownFormPassword1">Password</label>
                  <input type="password" required class="form-control" id="exampleDropdownFormPassword1" placeholder="Password">
                </div>
              <div>
                <button required type="submit" class="btn btn-primary">Sign in</button>
                <a href="http://"> Forgot Your Password?</a>
              </div>
              </form>
        </div>
        <div class="col-4">

            <div class="createAcount d-grid">
                <b>
                    New Customer?
                </b> 
                <div>
                    <span>Creating an account has many benefits:</span>
                    
                    <ul> 
                        <li>Check out faster</li>
                        <li>Keep more than one address</li>
                        <li>Track orders and more</li>
                    </ul>

                </div>

                <button required type="submit" class="btn btn-primary">Create An Account</button>
            </div>


        </div>
    </div>

</div>
    
@endsection