@extends('components.base')
@push('css')

<link rel="stylesheet" href="./css/busket.css">
@endpush

@section('content')

<div class="row">
    <div class="col-12  mt-3">
       <x-Tpage>
        Shopping Cart
       </x-Tpage>
    </div>
</div>
<div class="row m-5 ">
    <div class="col-12 gap-4 d-flex ">
        <div class="col-8">
            <div class="d-flex justify-content-between">
                <b>Item</b>
                <div class="wrappProd">
                    <span>Price</span>
                    <span>Qty</span>
                    <span>Subtotal</span>
                </div>
            </div>
            @for ($i =1 ; $i <5 ; $i++)
            <div class="aboutProd mt-2">
                <div>
                    <img src="" alt="">
                    <span class="TaboutProd">
                        MSI MEG Trident X 10SD-1012AU Intel i7 10700K, 2070 SUPER, 32GB RAM, 
                        1TB SSD, Windows 10 Home, Gaming Keyboard and Mouse 3 Years Warranty
                    </span>
                </div>
                <div>
                    <span class="price">1000</span>
                    <input type="number" min="1" value="1">
                    <span class="total">1000</span>
                </div>
              <x-itemOption>

                </x-itemOption>
            </div>
            @endfor
            <div class="WrappoptionBusket d-flex justify-content-between align-items-center mt-4  pt-4">
                <div class="d-flex">
                   <a href="http://" class="optionBusket">Continue Shopping</a>
                   <a href="http://" class="optionBusket">Clear Shopping Cart</a>
                </div>
                <a href="http://" class="optionBusket">Update Shopping Cart</a>

            </div>
        </div>
        <div class="col-4 d-flex flex-column ">
            <x-wrappBlueBG>
                <x-Tpage>Summary</x-Tpage>
                <div class="EstiShipp">
                        <span>Estimate Shipping and Tax</span>
                        <span>Enter your destination to get a shipping estimate.</span>
                </div>
                <div class="discountCode d-grid">
                    <span>
                        Apply Discount Code
                    </span>
                    <input type="text">
                </div>
                <div class="costs">
                    <span class="shipping">
                        <b>Subtotal</b>
                        <b>0000</b>
                    </span>
                    <span class="note">
                        <b>Shipping</b>
                        <b>0000</b>
                    </span>
                    <label>(Standard Rate - Price may vary depending on the item/destination. TECS Staff will contact you.)</label>
                    <span class="tax">
                        <b>Tax</b>
                        <b>0000</b>
                    </span>
                    <span  class="gst"> 
                        <b>GST</b>
                        <b>0000</b>
                    </span>
                    <span class="orederT">
                        <b>Order Total</b>
                        <b>0000</b>
                    </span>
                </div>
                <div class="buttons">
                    <a href="">
                    <x-AddCart>Proceed to Checkout</x-AddCart>
                    </a>
                    <a href="">
                        <x-PayPal>Check out with</x-PayPal>
                    </a>
                    <a href="">Check Out with Multiple Addresses</a>
                </div>
            </x-wrappBlueBG>
        </div>
    </div>
</div>

    
@endsection