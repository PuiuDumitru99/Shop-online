@extends('components.base')

@push('css')
    
<link rel="stylesheet" href="./css/contact.css">
@endpush

@section('content')
<div class="row">
    <div class="col-12 gap-4 d-flex">
        <div class="col-9 d-flex flex-column">
            <div class="divConUs d-grid mt-4 mb-5">
                <span>Contact Us</span>
                <span>
                    We love hearing from you, our Shop customers. Please contact us and we will make sure to get back to you as soon as we possibly can.
                </span>
            </div>
            <form class="float-left ContactForm">
                <div class="form-group">
                  <label for="exampleInputEmail1">Your Name</label>
                  <input type="text" required class="form-control" placeholder="Your Name">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Your Email </label>
                    <input type="email"  required class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Your Email">
                  </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Your Phone Number</label>
                  <input type="phone" required class="form-control" id="exampleInputPassword1" placeholder="Your Phone ">
                </div><br>
                <div class="form-group">
                    <span for="textArea">What’s on your mind?</span><br>
                   <textarea name="" id="textArea" cols="30" rows="10" placeholder="Jot us a note and we’ll get back to you as quickly as possible"></textarea>
                  </div>                
                <button type="submit" required class="btn btn-primary">Submit</button>
              </form>

        </div>
        <div class="col-3 d-flex">
            <div class="address">
                <div>
                    <label>Address:</label>
                    <span>1234 Street Adress City Address, 1234</span>
                </div>
                <div>
                    <label>Phone:</label>
                    <span>(00)1234 5678</span>
                </div>
                <div>
                    <label>We are open:</label>
                    <span>Monday - Thursday: 9:00 AM - 5:30 PM Friday 9:00 AM - 6:00 PM Saturday: 11:00 AM - 5:00 PM</span>
                </div>
                <div>
                    <label>E-mail:</label>
                    <a href="http://">shop@email.com</a>
                </div>

            </div>

        </div>
    </div>
</div>
@endsection
@push('js')
    
@endpush