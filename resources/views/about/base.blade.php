@extends('components.base')
@push('css')
<link rel="stylesheet" href="./css/about.css">
<link rel="stylesheet" href="./css/slick/slick.css">
<link rel="stylesheet" href="./css/slick/slick-theme.css">
@endpush

@section('content')
    <div class="row wrapper_About d-flex justify-content-around align-items-center">
        <div class="col-6 d-flex">
            <a href="/about">About Product</a>
            <a href="/details">Details</a>
            <a href="/specs">Specs</a>
        </div>
        <div class="col-6 d-flex justify-content-between align-items-center">
            <span class="d-flex  align-items-center tei">
                On Sale from <b class="ml-2">$3,299.00</b>
            </span>
            <input type="number" class="quantity" min="1" placeholder="1">
            <x-AddCart>Add to Cart</x-AddCart>
            <x-PayPal></x-PayPal>
        </div>
    </div>
</div>

<div class="aboutProduct">
    <div class="container">
    <div class="row d-flex ">
        <div class="col-6 d-flex flex-column  justify-content-between mt-2 mb-2 pr-5">
            <span class="addres_link">

            </span>
            <span class="Title_prod mt-3">
                MSI MPG Trident 3
            </span>
            <a class="mb-2" href="http://"> Be the first to review this product</a>
            @yield('firstPart')
            <span>
               <b> Have a Question?</b> 
                <a href="">Contact Us</a>
            </span>
                    <a href="" class="More_info">
                    + More information
                </a>                
           
        </div>
        <div class="col-6 d-flex p-3">
            <div class="d-flex  flex-column mt-5 mr-5  wrap_option">
                <a href=""></a>
                <a href=""></a>
                <a href=""></a>
            </div>
            <div class=" img_product d-flex">
                <img  src="./img/img_prod/image9.png" alt="">
                <img  src="./img/img_prod/image9.png" alt="">
            </div>
           
        </div>
    </div>
</div>
</div>
<div class="wrapperBanner">
    <div class="container">
    <div class="row">
        <div class="col-12 d-flex">
            <div class="info1 col-3 ">
                <b>
                    Outplay the Competittion
                </b>
                <span>
                    Experience a 40% boost in computing from last generation. 
                    MSI Desktop equips the 10th Gen. Intel® Core™ i7 processor with the upmost 
                    computing power to bring you an unparalleled gaming experience.
                </span>
                <span>
                    *Performance compared to i7-9700. Specs varies by model.
                </span>
        
            </div>
            <div class="col-9 banner p-3">
                <img src="./img/bilbord/wrapperBanner/Rectangle1.png" alt="">
                <img src="./img/bilbord/wrapperBanner/Rectangle1.png" alt="">
                <img src="./img/bilbord/wrapperBanner/Rectangle1.png" alt="">
            </div>
        </div>
    </div>
</div>
</div>
<div id="wrapperSuport">
    <div class="container" >
        <div class="row">
            <div class="col supportCenter ">
                <a href="">Product Support</a>
                <a href="">FAQ</a>
                <a href="">Our Buyer Guide</a>
            </div>
        </div>
    </div>
</div>

<div class="featues">
    <div class="container">
        <div class="row justify-content-center mb-5">
            <div class="col-4 d-flex flex-column mb-3 mt-3 justify-content-center align-items-center text-center title">
                <b>
                    Featues
                </b>
                <p>
                    The MPG series brings out the best in gamers by 
                    allowing full expression in color with advanced RGB 
                    lighting control and synchronization.
                </p>

            </div>
        </div>
        <div class="row listB" >
            <div class="col col-3 d-flex  flex-column" >
                <img src="./img/brand/intel.png" alt="">
                <span>Intel® Core™ i7 processor with the upmost computing power to bring you an unparalleled gaming experience.</span>
            </div>
            <div class="col col-3 d-flex  flex-column">
                <img src="./img/brand/nvidea.png" alt="">
                <span>The new GeForce® RTX SUPER™ Series has more cores and higher clocks for superfast performance compared to previous-gen GPUs.</span>
            </div >
            <div class="col col-3 d-flex  flex-column">
                <img src="./img/brand/ssd.png" alt="">
                <span> Unleash the full potential with the latest SSD technology, the NVM Express. 6 times faster than traditional SATA SSD.</span>
            </div>
            <div class="col col-3 d-flex  flex-column">
                <img src="./img/brand/ddr4.png" alt="">
                <span>
                    Featuring the latest 10th Gen Intel® Core™ processors, memory can support up to DDR4 2933MHz to delivers an unprecedented gaming experience.
                </span>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script src="./js/Mslick/slick.min.js">
</script>
<script src="./js/Mslick/slick.js"></script>

@endpush
    
