<nav class="black_wrapp">
    <div class="container">
      <div class="row">
        <div class="col-12 d-flex justify-content-between align-content-center ">
          <div class="calendar_wrapp d-flex justify-content-between align-content-center ">
           <select name="" id="" class="calendar">
              <option value="1">
                <span>Mon-Thu:</span>9:00 AM - 5:30 PM
              </option>
              <option value="2">
                <span>Fr:</span>9:00 AM - 6:00 PM
              </option>
              <option value="3">
                <span>Sat:</span>11:00 AM - 5:00 PM
              </option>
           </select>
          </div>
          <div class="addres">
            <span>Visit our showroom in 1234 Street Adress City Address, 1234 </span>
            <a href="/contactUs">
              Contact Us
            </a>
          </div>
          <div class="contact d-flex justify-content-center align-items-center">
              Call Us: <span class="number_phone">
                (00) 1234 5678
              </span>
              <a href="http://"></a>
              <a href="http://"></a>
            <div class="d-flex justify-content-center align-items-center"> 
            </div>
            </div>
        </div>
      </div>
    </div>
</nav>
<nav class="meniu d-flex justify-center align-items-center">
  <div class="container">
    <div class="row">
      <div class="col col-12 d-flex justify-content-between nowrap">
        <a href="/">
          <div class="logo">         
          </div>
        </a>
        <div class="listMeniu">
            <ul>
              <li><a href="">Laptops</a>
                <ul>
                  <li><a href="">Everyday Use Notebooks</a>
                    <ul>
                      <li><a href="http://"> MSI Workstation Series</a>
                        <ul>
                          <li><a href="http://">MSI WS Series (<span>12</span>)</a></li>
                          <li><a href="http://">MSI WT Series (<span>31</span>)</a></li>
                          <li><a href="http://">MSI WE Series (<span>7</span>)</a></li>
                        </ul>
                      </li>
                      <li><a href="">MSI Prestige Series</a></li>
                    </ul>
                  </li>
                  <li><a href="">MSI Workstation Series</a></li>
                  <li><a href="">MSI Prestige Series</a></li>
                  <li><a href="">Gaming Notebooks</a></li>
                  <li><a href="">Tablets And Pads</a></li>
                  <li><a href="">Netbooks</a></li>
                  <li><a href="">Infinity Gaming Notebooks</a></li>
                </ul>
              </li>
              <li><a href="">Desktop PCs</a></li>
              <li><a href="">Networking Devices</a></li>
              <li><a href="">Printers & Scanners</a></li>
              <li><a href="">PC Parts</a></li>
              <li><a href="">All Other Products</a></li>
              <li><a href="">Repairs</a></li>
              <li><a href="">
              <x-btnBlueBor>
                Our Deals
                </x-btnBlueBor>  
              </a></li>             
            </ul>              
        </div>
        <div class="busket_case d-flex align-items-center justify-content-around ">
          <div class="d-flex align-items-center wrappSearch">
            <input type="find" class="search">
            <span class="close">x</span>
          </div>
              <div class="busket">
                <a href="/busket"></a>
                <div class="wrapper_busket">
                  <div class=" d-flex flex-column align-items-center pt-2">
                    <b>My Cart</b>
                    <span>2 item in cart</span>
                    <x-btnBlueBor>View or Edit Your Cart</x-btnBlueBor>
                  </div>                   
                    @for ($item = 1; $item <=2; $item++)
                    <div class="d-flex justify-content-around align-items-center border "> 
                      <span>x1</span>
                      <img src="/img/img_prod/3.png" alt=""> 
                      <p>
                        EX DISPLAY : MSI Pro 16 Flex-036AU 15.6 MULTITOUCH All-In-On...
                      </p>
                      <x-itemOption></x-itemOption>
                    </div> 
                    @endfor
                  <div class=" d-flex flex-column justify-content-evenly">
                    <span class=" align-self-center ">Subtotal:<b> $499.00</b></span>
                    <a href=""><x-AddCart>Go to Checkout</x-AddCart></a>
                    <a href=""><x-PayPal>Check out with</x-PayPal></a>
                  </div>
                </div>
              </div>
          <div class="user">
            <div class="userOption">
              <a href="http://">My Account</a>
              <a href="http://"> My Wish List(<span>0</span> )</a>
              <a href="http://">Compare (<span>0</span> )</a>
              <a href="http://">Create an Account</a>
              <a href="/login">Sign In</a>
            </div>            
          </div>     
        </div>
      </div>     
    </div>
</nav>


