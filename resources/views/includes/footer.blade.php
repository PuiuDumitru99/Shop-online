<footer>
    <div class="container d-flex flex-column justify-content-between pt-5">
        <div class="row">
            <div class="col-12 d-flex justify-content-between align-items-center"> 
                <div class="col d-grid ">
                    <span class="sNews">Sign Up To Our Newsletter.</span>
                    <span class="slogan">Be the first to hear about the latest offers.</span>
                </div>
                <div class="subscribe col col-6 d-flex  justify-content-around align-items-end">
                    <input type="text" placeholder="Your Email">
                    <button class="btnSubscribe">
                        Subscribe
                    </button>
                </div>               
            </div>
        </div>
        <div class="row footer_list pt-5">
            <div class="col">
                <span>Information</span>
                <ul>
                   <li><a href="">Text</a></li>
                   <li><a href="">Text</a></li>
                   <li><a href="">Text</a></li>
                   <li><a href="">Text</a></li>
                   <li><a href="">Text</a></li>
                   <li><a href="">Text</a></li>
                   <li><a href="">Text</a></li>
                   <li><a href="">Text</a></li>
                   <li><a href="">Text</a></li>
                </ul>
            </div>
            <div class="col">
                <span>PC Parts</span>
                <ul>
                  <li><a href="">Text_____</a></li>
                  <li><a href="">Text_____</a></li>
                  <li><a href="">Text_____</a></li>
                  <li><a href="">Text_____</a></li>
                  <li><a href="">Text_____</a></li>
                  <li><a href="">Text_____</a></li>
                  <li><a href="">Text_____</a></li>
                  <li><a href="">Text_____</a></li>
                  <li><a href="">Text_____</a></li>
                  <li><a href="">Text_____</a></li>
                </ul>
            </div>
            <div class="col">
                <span>Desktop PCs</span>
                <ul>
                   <li><a href="">Text_____</a></li>
                   <li><a href="">Text_____</a></li>
                   <li><a href="">Text_____</a></li>
                   <li><a href="">Text_____</a></li>
                   <li><a href="">Text_____</a></li>
                   <li><a href="">Text_____</a></li>
                </ul>
            </div>
            <div class="col">
                <span>Laptops</span>
                <ul>
                    <li><a href="">Text_____</a></li>
                    <li><a href="">Text_____</a></li>
                    <li><a href="">Text_____</a></li>
                    <li><a href="">Text_____</a></li>
                    <li><a href="">Text_____</a></li>
                    <li><a href="">Text_____</a></li>
                </ul>
            </div>
            <div class="col">
                <span>Address</span>
                <ul>
                    <li><a href="">Text_____</a></li>
                    <li><a href="">Text_____</a></li>
                    <li><a href="">Text_____</a></li>
                    <li><a href="">Text_____</a></li>
                    <li><a href="">Text_____</a></li>
                    <li><a href="">Text_____</a></li>
                    <li><a href="">Text_____</a></li>
                </ul>
            </div>
        </div>
        <div class="row payMethod">
            <div class="col-12 d-flex justify-content-between align-items-center">
                <div class="social_media">
                    <a href=""></a>
                    <a href=""></a>
                </div>
                <div class="typePay">
                    <a href=""><img src="./img/payM/american-express.png" alt=""></a>
                    <a href=""><img src="./img/payM/discover.png" alt=""></a>
                    <a href=""><img src="./img/payM/maestro.png" alt=""></a>
                    <a href=""><img src="./img/payM/paypal.png" alt=""></a>
                    <a href=""><img src="./img/payM/visa.png" alt=""></a>
                </div>
                <div class="copyright">
                    <span>
                        ©  This is my App
                    </span>
                </div>

            </div>
        </div>
    </div>
</footer>