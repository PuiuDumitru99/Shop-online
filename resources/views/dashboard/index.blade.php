@extends('components.base')

@push('css')
<link rel="stylesheet" href="/css/dashboard.css">
    
@endpush
@section('content')

<x-Tpage>
    My Dashboard
</x-Tpage>

<div class="row min-vh-100 mt-5 gap-5">
    <div class="col-4">
        <x-wrappBlueBG id="wrappBlueBG">
            <div class=" d-flex flex-column justify-content-evenly">
                <a href="http://">Account Dashboard</a>
                <a href="http://">Account Information</a>
                <a href="http://">Address Book</a>
                <a href="http://">My Orders</a>
            </div>
            <hr>
            <div class="d-flex flex-column justify-content-evenly">
                <a href="http://">My Downloadable Products</a>
                <a href="http://">Stored Payment Methods</a>
                <a href="http://">Billing Agrements</a>
                <a href="http://">My Wish List</a>
            </div>
            <hr>
            <div class="d-flex flex-column justify-content-evenly">
                <a href="http://">My Product Reviews</a>
                <a href="http://">Newsletter Subscriptions</a>
            </div>            
            <div class="d-flex flex-column justify-content-evenly align-items-center border-top-2">
                <b>Compare Products</b>
                <span>You have no items in your wish list.</span>
            </div>
            <div class="d-flex flex-column justify-content-evenly align-items-center ">
                <b>My Wish List</b>
                <span>You have no items in your wish list.</span>
            </div>
        </x-wrappBlueBG>
    </div>
    <div class="col d-flex flex-column justify-content-start " id="details">   
        <span>
            Account Information
        </span>        
            <div class="row border-top mt-2 mb-5 gap-5 pt-3">
                <div class="col-4 d-flex flex-column justify-content-evenly ">
                    <b>Contact Information</b>
                    <P>Alex Driver ExampeAdress@gmail.com</P>
                    <div class="d-flex justify-content-evenly ">
                        <a href="http://">Edit</a>
                        <a href="http://">Change Password</a>
                    </div>
                </div>
                <div class="col ">
                   <b>Newsletters</b> 
                   <p>You don't subscribe to our newsletter.</p>
                    <a href="http://">Edit</a>
                </div>
            </div>
            <div>
                <span>Address Book </span>  <a href="http://"> Menage Addresses</a>
            </div>
            <div class="row border-top mt-2 mb-5 pt-3 gap-5 ">
                <div class="col-4 d-flex flex-column justify-content-evenly ">
                    <b>Default Billing Address</b> 
                    <p>You have not set a default billing address.</p>
                    <a href="http://">Edit Address</a>

                </div>
                <div class="col d-flex flex-column justify-content-evenly ">
                   <b>Default Shipping Address</b>  
                   <p>You have not set a default shipping address.</p>
                   <a href="http://">Edit Address</a>

                </div>
                
            </div>
      
    </div>



</div>



@endsection