<?php

use App\Http\Controllers\AboutController;
use App\Http\Controllers\ContactUs;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\laptopsControllser;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\shoppingCartController;
use Illuminate\Support\Facades\Route;

use Whoops\Run;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/



Route::get('/',[HomeController::class,'index'])->name('home.page');
Route::get('/about',[AboutController::class,'index'])->name('about.page');
Route::get('/details',[AboutController::class,'details'])->name('about.details');
Route::get('/specs',[AboutController::class,'specs'])->name('about.specs');
Route::get('/contactUs',[ContactUs::class,'index'])->name('contactUs');
Route::get('/login',[LoginController::class,'index'])->name('login.index');
Route::get('/busket',[shoppingCartController::class,'index'])->name('shoppingCart.index');
route::get('/laptops',[laptopsControllser::class,'index'])->name('laptops.index');
route::get('/dashboard',[DashboardController::class, 'index'])->name('dashboard.index');


